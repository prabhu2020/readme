Flagging Claims That Are Labeled As Potentially Preventable Readmissions According to 3M
==========================
 ###### Have a suggestion on how to make this code better? Follow these instuctions to suggest an update: https://confluence.centene.com/display/CHA/Submitting+Suggestions

Other Names For This Term
-----------------
Potentially Preventable Readmission, PPR

Business Definition
-----------------
Claims that are deemed potentially preventable according to the logic 3M provides

Business Guidance
-----------------
Each month, we (Centene) runs a batch of our Inpatient claims through 3M's proprietary Potentially Preventable (PPR) tool.  The tool outputs which of our claims either: 1) Led to at least one potentially preventable readmission (Initial Admission, labeled 'IA'), 2) Did not lead to any potentially preventable readmissions (Other Admission, labeled 'OA'), or 3) Was in itself a potentially preventable readmission (labeled 'RA').<br/>
<br/>
For additional documentation on how the 3M Potentially Preventable Events tool works, please visit: https://confluence.centene.com/display/CHA/3M+Preventables

Data Domain
-----------------
Clinical

Access Requirements
-----------------
MMM_GPS_ACCESS_OWN

Table Usage
-----------------
MMM_PPR_CONTROL
MMM_PPR_OUTPUT

Other Notes
-----------------
In order to run this query, you must determine which criteria you are interested in:<br/>
  - What time period ar eyou interested in? (Must be 1 year, starting on the 1st of a month)<br/>
  - Interested in 15 or 30 day readmissions?<br/>
  - Do you want to include or exclude mental health claims?<br/>
 
Frequently Asked Questions
-----------------
**What if I want to look at more than one year timeframe?**

**What is the formula to calculate Potentially Preventable Readmission (PPR) Rates?**

**Is there special logic I need to use to trend rates?**<br/>
Yes! You must adjust your calculations based on the "riskiness" of your membership.  Visit this link to learn more: TBD
 
  